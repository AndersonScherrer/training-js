# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Training git flow
* Training JavaScript ES6
* [Learn ES6](http://es6-features.org/#Constants)

### Developers guidelines ###

* Clone it:
* Checkout to development: git checkout development
* At Start of Day or Before close your feature: git pull 
* Create Local Feature branch: git checkout -b feature-***
* If is necessary share your feature branch, then: git push -f
* Writing tests
* Writing code
* When necessary pull during development time: git pull --rebase origin development
* When necessary change branch during development time without a commit: git stash 
* And before back to feature-branch: git stash pop
* Add your changes to staging area: git add -A or git add -p 
* Commit your code: git commit -m "Your Short and Awsome Message" or git commit --amend
* Merge your feature branch to development: git checkout development && git merge --no-ff feature-*** 
* Push development: git push origin development
* Delete feature branch: git branch -d feature-***
